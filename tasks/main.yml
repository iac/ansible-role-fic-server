---
- name: "Ensure var_base dir exists"
  ansible.builtin.file:
    path: "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}"
    state: directory
    owner: "{{ ansible_user }}"

- name: "setup fic-{{ fic_name }} network between containers"
  community.general.docker_network:
    name: "fic-{{ fic_name }}"

- name: "launch fic-admin container {{ fic_name }}"
  community.general.docker_container:
    name: "fic-admin-{{ fic_name }}"
    image: "nemunaire/fic-admin:{{ image_tag }}"
    pull: true
    init: true
    networks_cli_compatible: true
    networks:
      - name: "fic-{{ fic_name }}"
    volumes:
      - "/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/raw_files:/mnt/fic"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/dashboard:/srv/DASHBOARD"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/files:/srv/FILES"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/pki:/srv/PKI"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/settings:/srv/SETTINGS"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/submissions:/srv/submissions:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/sync:/srv/SYNC"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/teams:/srv/TEAMS"
    state: started
    restart_policy: unless-stopped
    command: ["-bind=:8081", "-baseurl={% if current_fic != '' %}/{{ current_fic }}{% endif %}/admin/", "-localimport=/mnt/fic", "-localimportsymlink", "-timestampCheck=/srv/submissions"]
    memory: 1G
    memory_swap: 1G
    cap_drop: ALL
    env:
      FIC_4REAL: "{% if challenge_mode is defined and challenge_mode %}on{% else %}off{% endif %}"
      MYSQL_HOST: /var/run/mysqld/mysqld.sock
      MYSQL_USER: "{{ database.username }}"
      MYSQL_PASSWORD: "{{ database.password }}"
      MYSQL_DATABASE: "fic{{ fic_name }}{% if custom_db_suffix is defined %}_{{ custom_db_suffix }}{% endif %}"
    log_driver: syslog
    log_options:
      syslog-address: unixgram:///dev/log
      syslog-facility: daemon
      tag: "fic-admin-{{ fic_name }}"
  tags:
    - live
    - live-admin

- name: "launch fic-evdist container {{ fic_name }}"
  community.general.docker_container:
    name: "fic-evdist-{{ fic_name }}"
    image: "nemunaire/fic-evdist:{{ image_tag }}"
    pull: true
    networks_cli_compatible: true
    networks:
      - name: "fic-{{ fic_name }}"
    volumes:
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/settings:/srv/SETTINGS"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/settingsdist:/srv/SETTINGSDIST"
    state: started
    restart_policy: unless-stopped
    memory: 256M
    cap_drop: ALL
    env:
      FIC_BASEURL: "{% if current_fic != '' %}/{{ current_fic }}{% endif %}/"
      MYSQL_HOST: /var/run/mysqld/mysqld.sock
      MYSQL_USER: "{{ database.username }}"
      MYSQL_PASSWORD: "{{ database.password }}"
      MYSQL_DATABASE: "fic{{ fic_name }}{% if custom_db_suffix is defined %}_{{ custom_db_suffix }}{% endif %}"
    log_driver: syslog
    log_options:
      syslog-address: unixgram:///dev/log
      syslog-facility: daemon
      tag: "fic-backend-{{ fic_name }}"
  tags:
    - live
    - live-backend

- name: "launch fic-backend container {{ fic_name }}"
  community.general.docker_container:
    name: "fic-backend-{{ fic_name }}"
    image: "nemunaire/fic-backend:{{ image_tag }}"
    pull: true
    networks_cli_compatible: true
    networks:
      - name: "fic-{{ fic_name }}"
    volumes:
      - "/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/settingsdist:/srv/SETTINGSDIST:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/submissions:/srv/submissions"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/teams:/srv/TEAMS"
    state: started
    restart_policy: unless-stopped
    memory: 256M
    cap_drop: ALL
    env:
      FIC_BASEURL: "{% if current_fic != '' %}/{{ current_fic }}{% endif %}/"
      MYSQL_HOST: /var/run/mysqld/mysqld.sock
      MYSQL_USER: "{{ database.username }}"
      MYSQL_PASSWORD: "{{ database.password }}"
      MYSQL_DATABASE: "fic{{ fic_name }}{% if custom_db_suffix is defined %}_{{ custom_db_suffix }}{% endif %}"
    log_driver: syslog
    log_options:
      syslog-address: unixgram:///dev/log
      syslog-facility: daemon
      tag: "fic-backend-{{ fic_name }}"
  tags:
    - live
    - live-backend

- name: "launch fic-dashboard container {{ fic_name }}"
  community.general.docker_container:
    name: "fic-dashboard-{{ fic_name }}"
    image: "nemunaire/fic-dashboard:{{ image_tag }}"
    pull: true
    networks_cli_compatible: true
    networks:
      - name: "fic-{{ fic_name }}"
    volumes:
      - "/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/dashboard:/srv/DASHBOARD"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/settingsdist:/srv/SETTINGSDIST:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/teams:/srv/TEAMS:ro"
    state: started
    restart_policy: unless-stopped
    memory: 256M
    cap_drop: ALL
    env:
      FIC_BASEURL: "{% if current_fic != '' %}/{{ current_fic }}{% endif %}/dashboard/"
      MYSQL_HOST: /var/run/mysqld/mysqld.sock
      MYSQL_USER: "{{ database.username }}"
      MYSQL_PASSWORD: "{{ database.password }}"
      MYSQL_DATABASE: "fic{{ fic_name }}{% if custom_db_suffix is defined %}_{{ custom_db_suffix }}{% endif %}"
    log_driver: syslog
    log_options:
      syslog-address: unixgram:///dev/log
      syslog-facility: daemon
      tag: "fic-dashboard-{{ fic_name }}"
  tags:
    - live
    - live-dashboard

- name: "launch fic-frontend container {{ fic_name }}"
  community.general.docker_container:
    name: "fic-frontend-{{ fic_name }}"
    image: "nemunaire/fic-frontend:{{ image_tag }}"
    pull: true
    command: ["-bind=:8080", "-startedFile=STARTINGBLOCK/started"]
    networks_cli_compatible: true
    networks:
      - name: "fic-{{ fic_name }}"
    volumes:
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/settingsdist:/srv/SETTINGSDIST:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/startingblock:/srv/STARTINGBLOCK"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/teams:/srv/TEAMS:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/submissions:/srv/submissions"
    state: started
    restart_policy: unless-stopped
    memory: 256M
    cap_drop: ALL
    log_driver: syslog
    log_options:
      syslog-address: unixgram:///dev/log
      syslog-facility: daemon
      tag: "fic-frontend-{{ fic_name }}"
  tags:
    - live
    - live-frontend

- name: "launch fic-qa container {{ fic_name }}"
  community.general.docker_container:
    name: "fic-qa-{{ fic_name }}"
    image: "nemunaire/fic-qa:{{ image_tag }}"
    pull: true
    networks_cli_compatible: true
    networks:
      - name: "fic-{{ fic_name }}"
    volumes:
      - "/var/run/mysqld/mysqld.sock:/var/run/mysqld/mysqld.sock"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/teams:/srv/TEAMS:ro"
    state: started
    restart_policy: unless-stopped
    memory: 256M
    cap_drop: ALL
    env:
      FIC_BASEURL: "{% if current_fic != '' %}/{{ current_fic }}{% endif %}/qa/"
      MYSQL_HOST: /var/run/mysqld/mysqld.sock
      MYSQL_USER: "{{ database.username }}"
      MYSQL_PASSWORD: "{{ database.password }}"
      MYSQL_DATABASE: "fic{{ fic_name }}{% if custom_db_suffix is defined %}_{{ custom_db_suffix }}{% endif %}"
    log_driver: syslog
    log_options:
      syslog-address: unixgram:///dev/log
      syslog-facility: daemon
      tag: "fic-qa-{{ fic_name }}"
  tags:
    - live
    - live-qa

- name: "launch fic-nginx container {{ fic_name }}"
  community.general.docker_container:
    name: "fic-nginx-{{ fic_name }}"
    image: "nemunaire/fic-nginx:{{ image_tag }}"
    pull: true
    networks_cli_compatible: true
    networks:
      - name: "fic-{{ fic_name }}"
    volumes:
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/raw_files:/mnt/fic:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/files:/srv/FILES:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/settingsdist:/srv/SETTINGS:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/startingblock:/srv/STARTINGBLOCK:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/sync:/srv/SYNC:ro"
      - "{{ var_base }}{% if current_fic != '' %}/{{ current_fic }}{% endif %}/teams:/srv/TEAMS:ro"
    state: started
    restart_policy: unless-stopped
    memory: 256M
    published_ports:
      - "127.0.0.1:{{ endpoint }}:80"
    env:
      FIC_BASEURL: "{% if current_fic != '' %}/{{ current_fic }}{% endif %}/"
      FIC_BASEURL2: "{% if current_fic != '' %}/{{ current_fic }}{% endif %}/"
      HOST_ADMIN: "fic-admin-{{ fic_name }}:8081"
      HOST_DASHBOARD: "fic-dashboard-{{ fic_name }}:8082"
      HOST_FRONTEND: "fic-frontend-{{ fic_name }}:8080"
      HOST_QA: "fic-qa-{{ fic_name }}:8083"
    log_driver: syslog
    log_options:
      syslog-address: unixgram:///dev/log
      syslog-facility: daemon
      tag: "fic-frontend-{{ fic_name }}"
  tags:
    - live
    - live-nginx
